package com.confusedpenguins.breadcrumbs.activity;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.adapter.CrumbManager;
import com.confusedpenguins.breadcrumbs.application.location.GeofenceTransitionsIntentService;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.confusedpenguins.breadcrumbs.application.constants.ConstLocation;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.application.location.BreadcrumbConnectionFailedListener;
import com.confusedpenguins.breadcrumbs.application.location.BreadcrumbLocationListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

/**
 * Created by Andrew on 9/2/2017.
 */

public class CreateCrumbActivity extends DialogFragment implements GoogleApiClient.ConnectionCallbacks {
    private EditText mEditText;
    private boolean mRequestingLocationUpdates = true;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private BreadcrumbLocationListener breadcrumbLocationListener;
    private final int TAG_CODE_PERMISSION_LOCATION = 1;
    private String parentId;
    private PendingIntent mGeofencePendingIntent;

    private CrumbType crumbType;

    BreadCrumb breadCrumb = null;

    private UseCaseFactory factory;

    public CreateCrumbActivity() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static CreateCrumbActivity newInstance(CrumbType crumbType, String parentId) {
        CreateCrumbActivity frag = new CreateCrumbActivity();
        Bundle args = new Bundle();
        args.putString("type", crumbType.name());
        args.putString("parentId", parentId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return getActivity().getLayoutInflater().inflate(R.layout.dialog_create_breadcrumb, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.factory = new UseCaseFactory(getActivity());

        // Get field from view
        mEditText = (EditText) view.findViewById(R.id.txt_your_name);

        this.parentId = getArguments().getString("parentId", null);

        this.crumbType = CrumbType.get(getArguments().getString("type", null));

        if (CrumbType.TEXT.equals(this.crumbType)) {
            // Show soft keyboard automatically and request focus to field
            mEditText.requestFocus();
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        getDialog().setTitle(getActivity().getString(R.string.create_breadcrumb_header));

        Button mButton = (Button) view.findViewById(R.id.dialog_create_crumb);
        mButton.setOnClickListener(v -> saveNote());

        this.breadcrumbLocationListener = new BreadcrumbLocationListener();

        init();
    }

    private void init() {
        this.mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new BreadcrumbConnectionFailedListener())
                .build();

        mGoogleApiClient.connect();
    }

    protected void startLocationUpdates() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(ConstLocation.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(ConstLocation.FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, TAG_CODE_PERMISSION_LOCATION);
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this.breadcrumbLocationListener);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, TAG_CODE_PERMISSION_LOCATION);
            return;
        }
        this.breadcrumbLocationListener.setTempLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, breadcrumbLocationListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        if (getActivity() instanceof CrumbManager) {
            ((CrumbManager) getActivity()).onCreateBreadcrumb(this.breadCrumb);
        }

        super.onDismiss(dialog);
        stopLocationUpdates();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case TAG_CODE_PERMISSION_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.startLocationUpdates();
                }
                return;
            }
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(getActivity(), GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private void closeDialog() {
        mEditText.setText("");
        stopLocationUpdates();
        dismiss();
    }

    private void saveNote() {
        String note = mEditText.getText().toString();

        if (note == null || note.isEmpty()) {
            return;
        }

        Location mCurrentLocation = this.breadcrumbLocationListener.getLocation();

        this.breadCrumb = new BreadCrumb();
        this.breadCrumb.setTitle("Hello World");
        this.breadCrumb.setContent(note);

        if (this.parentId != null) {
            this.breadCrumb.setParent(parentId);
        }

        if (mCurrentLocation != null) {
            this.breadCrumb.setLoc(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }

        this.breadCrumb = factory.newSaveBreadcrumbUseCase(this.breadCrumb).execute();

        ArrayList<BreadCrumb> crumbs = new ArrayList<>();
        crumbs.add(this.breadCrumb);

        this.factory.newAddGeofencesUseCase(crumbs, getGeofencePendingIntent(), createOnSuccessListener(), createOnFailureListener()).execute();

        if (this.breadCrumb != null) {
            closeDialog();
        }
    }

    private OnSuccessListener createOnSuccessListener() {
        return o -> {
            Log.i(CreateCrumbActivity.class.getCanonicalName(), "Created new Geo Fence");
        };
    }

    private OnFailureListener createOnFailureListener() {
        return e -> {
            Log.e(CreateCrumbActivity.class.getCanonicalName(), "Failed to create Geo Fence");
            Log.e(CreateCrumbActivity.class.getCanonicalName(), e.getMessage());
        };
    }
}