package com.confusedpenguins.breadcrumbs.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.utils.StringUtils;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class BreadcrumbMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    public static final String INTENT_BREADCRUMB = "BREADCRUMB";
    public static final String INTENT_BREADCRUMB_ARRAY = "BREADCRUMB_ARRAY";
    private GoogleMap mMap;
    private BreadCrumb breadCrumb;
    private ArrayList<BreadCrumb> breadCrumbs;

    private HashMap<String, BreadCrumb> markers = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breadcrumb_map);

        this.breadCrumb = (BreadCrumb) this.getIntent().getSerializableExtra(INTENT_BREADCRUMB);
        this.breadCrumbs = (ArrayList<BreadCrumb>) this.getIntent().getSerializableExtra(INTENT_BREADCRUMB_ARRAY);

        if (this.breadCrumb == null && this.breadCrumbs == null) {
            Log.e(BreadcrumbMapActivity.class.getName(), "No Payload Found");
            this.finish();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void createMarker(BreadCrumb breadCrumb) {
        LatLng location = new LatLng(breadCrumb.getLatitude(), breadCrumb.getLongitude());
        Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(breadCrumb.getContent()));
        this.markers.put(marker.getId(), breadCrumb);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);

//        if (this.breadCrumb != null) {
//            this.createMarker(this.breadCrumb);
//
//            TextView textView = (TextView) this.findViewById(R.id.activity_breadcrumb_content);
//            textView.setText(this.breadCrumb.getContent());
//            textView.setVisibility(View.VISIBLE);
//
//            LatLng location = new LatLng(this.breadCrumb.getLatitude(), this.breadCrumb.getLongitude());
//            CameraPosition cameraPosition = new CameraPosition.Builder()
//                    .target(location)      // Sets the center of the map to location user
//                    .zoom(17)                   // Sets the zoom
//                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
//                    .build();                   // Creates a CameraPosition from the builder
//            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        } else if (this.breadCrumbs != null) {
//            for (BreadCrumb crumb : this.breadCrumbs) {
//                this.createMarker(crumb);
//            }
//
//        }
    }

    public void clickContent(View view) {
        if (StringUtils.isUrl(this.breadCrumb.getContent())) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(this.breadCrumb.getContent()));
            startActivity(i);
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
//        TextView textView = (TextView) this.findViewById(R.id.activity_breadcrumb_content);
//        textView.setText(this.markers.get(marker.getId()).getContent());
//        textView.setVisibility(View.VISIBLE);
//
//        LatLng location = new LatLng(this.markers.get(marker.getId()).getLatitude(), this.markers.get(marker.getId()).getLongitude());
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(location)      // Sets the center of the map to Mountain View
//                .zoom(12)                   // Sets the zoom
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
//                .build();                   // Creates a CameraPosition from the builder
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        invalidateOptionsMenu();
        return true;
    }

    @Override
    public void onBackPressed() {
//        TextView textView = (TextView) this.findViewById(R.id.activity_breadcrumb_content);
//        if (textView.getVisibility() == View.VISIBLE && this.breadCrumbs != null) {
//            textView.setVisibility(View.GONE);
//
//            CameraUpdate zoom = CameraUpdateFactory.zoomTo(5);
//            mMap.animateCamera(zoom);
//        } else {
//            this.finish();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.breadCrumb != null) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.map_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_map_afterthought:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
