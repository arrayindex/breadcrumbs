package com.confusedpenguins.breadcrumbs.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by andrew on 1/14/18.
 */

public class FullMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private UseCaseFactory factory;
    private ArrayList<BreadCrumb> breadCrumbs;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng defaultView;

    private final int CLOSE = 17;
    private final int FAR = 1;

    private HashMap<String, Marker> markers = new HashMap<>();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        factory = new UseCaseFactory(this);

        this.setContentView(R.layout.activity_breadcrumb_map);
        this.init();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker == null) {
            return false;
        }

        focusMap(marker.getPosition(), CLOSE);
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        drawCrumbs();
    }

    private void init() {
        if (!loadBreadcrumbs()) {
            finish();
        }

        if (this.breadCrumbs.isEmpty()) {
            // Do something for empty lists
        }

        if (!initMap()) {
            finish();
        }
    }

    private boolean initMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        if (mapFragment == null) {
            return false;
        }

        mapFragment.getMapAsync(this);

        return true;
    }

    private boolean drawCrumbs() {
        if (this.breadCrumbs == null) {
            return false;
        }

        for (BreadCrumb crumb : this.breadCrumbs) {
            this.createMarker(crumb);
        }

        this.defaultView = factory.newFindCenterPointUseCase(this.breadCrumbs).execute();

        if (this.defaultView == null) {
            return false;
        }

        focusMap(this.defaultView, FAR);

        return true;
    }

    private void createMarker(BreadCrumb breadCrumb) {
        LatLng location = new LatLng(breadCrumb.getLatitude(), breadCrumb.getLongitude());
        Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(breadCrumb.getContent()));
        this.markers.put(breadCrumb.getAndroidId(), marker);
    }

    private boolean loadBreadcrumbs() {
        if (factory == null) {
            return false;
        }

        this.breadCrumbs = this.factory.newLoadBreadcrumbUseCase(null).execute();

        if (this.breadCrumbs == null) {
            return false;
        }

        return true;
    }

    private void focusMap(LatLng latLng, int zoom) {
        if (mMap == null) {
            return;
        }

        LatLng location = new LatLng(latLng.latitude, latLng.longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to location user
                .zoom(zoom)                   // Sets the zoom
                .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private boolean isZoomed() {
        if (this.mMap == null) {
            Log.w(FullMapActivity.class.getCanonicalName(), "mMap is null");
            return false;
        }

        if (this.mMap.getCameraPosition().zoom >= 10) {
            return true;
        }

        return false;
    }



    @Override
    public void onBackPressed() {
        if (isZoomed()) {
            focusMap(this.defaultView, FAR);
            return;
        }

        finish();
    }
}
