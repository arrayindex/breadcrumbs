package com.confusedpenguins.breadcrumbs.activity;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.adapter.BreadcrumbAdapter;
import com.confusedpenguins.breadcrumbs.adapter.ClickListener;
import com.confusedpenguins.breadcrumbs.adapter.CrumbManager;
import com.confusedpenguins.breadcrumbs.adapter.RecyclerTouchListener;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListBreadcrumbsActivity extends AppCompatActivity implements CrumbManager {
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private UseCaseFactory factory;
    private RecyclerView mRecyclerView;
    private BreadcrumbAdapter mAdapter;
    private ArrayList<BreadCrumb> breadCrumbs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_breadcrumbs);

        this.factory = new UseCaseFactory(this);

        setupRecycler();
        updateListView(null);
        decideCameraFAB();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.menu_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        if (searchViewAndroidActionBar != null) {
            searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    ListBreadcrumbsActivity.this.updateListView(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    ListBreadcrumbsActivity.this.updateListView(query);
                    return true;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateBreadcrumb(BreadCrumb breadCrumb) {
        addCrumb(breadCrumb);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_map_notes:
                Intent intent = new Intent(this, FullMapActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void decideCameraFAB() {
        if (!this.factory.newCheckCameraAvailableUseCase().execute()) {
            this.findViewById(R.id.floating_action_button_with_listview_camera).setVisibility(View.GONE);
        }
    }

    public void createCrumb(View view) {
        FragmentManager fm = getSupportFragmentManager();
        CreateCrumbActivity editNameDialogFragment = CreateCrumbActivity.newInstance(CrumbType.TEXT, null);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    private void showEmptyView() {
        if (this.breadCrumbs == null || this.breadCrumbs.isEmpty()) {
            this.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            this.findViewById(R.id.recycler_view_layout_recycler).setVisibility(View.GONE);
        } else {
            this.findViewById(R.id.empty_view).setVisibility(View.GONE);
            this.findViewById(R.id.recycler_view_layout_recycler).setVisibility(View.VISIBLE);
        }
    }

    private void setupRecycler() {
        mAdapter = new BreadcrumbAdapter(factory);
        mAdapter.setBreadCrumbs(this.breadCrumbs);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_layout_recycler);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent intent = new Intent(ListBreadcrumbsActivity.this, ViewBreadcrumbActivity.class);
                intent.putExtra(BreadcrumbMapActivity.INTENT_BREADCRUMB, ListBreadcrumbsActivity.this.mAdapter.getBreadcrumb(position).getAndroidId());
                ListBreadcrumbsActivity.this.startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListBreadcrumbsActivity.this);
                builder.setTitle(breadCrumbs.get(position).getContent());
                builder.setMessage(R.string.activity_list_alert_delete_crumb);
                builder.setIcon(R.drawable.ic_archive_black_24dp);

                builder.setPositiveButton(R.string.activity_list_alert_confirm, (dialog, id) -> {
                    BreadCrumb breadCrumb = breadCrumbs.get(position);
                    boolean completed = factory.newArchiveBreadcrumbUseCase(breadCrumb).execute();
                    if (completed) {
                        mAdapter.remove(position);
                        mAdapter.notifyItemRemoved(position);
                    }
                });

                builder.setNegativeButton(R.string.activity_list_alert_cancel, (dialog, id) -> {

                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }));
    }

    private void updateListView(String query) {
        breadCrumbs = factory.newLoadBreadcrumbUseCase(query).execute();

        showEmptyView();

        this.mAdapter.setBreadCrumbs(breadCrumbs);
        this.mAdapter.notifyDataSetChanged();
    }

    private void addCrumb(BreadCrumb breadCrumb) {
        if (breadCrumb == null) {
            return;
        }

        showEmptyView();

        this.mAdapter.add(breadCrumb);
        this.mAdapter.notifyItemInserted(0);

        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        layoutManager.scrollToPositionWithOffset(0, 0);
    }


    public void takePicture(View view) {
//        FragmentManager fm = getSupportFragmentManager();
//        CreateCrumbActivity editNameDialogFragment = CreateCrumbActivity.newInstance(CrumbType.IMAGE, null);
//        editNameDialogFragment.show(fm, "fragment_edit_name");
        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {

        File photoFile = factory.newCreateImageFileUseCase().execute();
        this.mCurrentPhotoPath = photoFile.getAbsolutePath();

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) == null) {
            return;
        }

        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(this, "com.confusedpenguins.breadcrumbs.fileprovider", photoFile);

            Log.i(ListBreadcrumbsActivity.class.getCanonicalName(), photoURI.toString());

            takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    ImageView mImageView = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mImageView = (ImageView) findViewById(R.id.imageView);


        if (requestCode == REQUEST_IMAGE_CAPTURE && requestCode == RESULT_CANCELED) {
            Log.d(ListBreadcrumbsActivity.class.getCanonicalName(), "Camera Intent Response: " + resultCode);
        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.d(ListBreadcrumbsActivity.class.getCanonicalName(), "Looks like it works");
            galleryAddPic();
            setPic();
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        Bitmap bitmap = factory.newLoadBitmapUseCase(null).execute();
        if (bitmap == null) {
            return;
        }
        mImageView.setImageBitmap(bitmap);
    }

    String mCurrentPhotoPath;
}
