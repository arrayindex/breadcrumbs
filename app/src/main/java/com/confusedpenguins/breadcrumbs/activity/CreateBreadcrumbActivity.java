package com.confusedpenguins.breadcrumbs.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.application.constants.ConstLocation;
import com.confusedpenguins.breadcrumbs.application.dialog.ActionCompletedModal;
import com.confusedpenguins.breadcrumbs.application.location.BreadcrumbConnectionFailedListener;
import com.confusedpenguins.breadcrumbs.application.location.BreadcrumbLocationListener;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;

/**
 * Created by andrew on 1/17/18.
 */

public class CreateBreadcrumbActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {
    private static final int TAG_CODE_PERMISSION_LOCATION = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;

    private BreadcrumbLocationListener breadcrumbLocationListener;
    private UseCaseFactory factory;
    private BreadCrumb breadCrumb;
    private String parentId;
    private String mCurrentPhotoPath;
    private boolean mRequestingLocationUpdates = true;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private EditText mEditText;
    private Button mCreateCrumbButton;
    private ImageView mImageView = null;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_breadcrumb);

        this.mEditText = (EditText) findViewById(R.id.create_crumb_text);
        this.mImageView = (ImageView) findViewById(R.id.imageView);
        this.mCreateCrumbButton = (Button) findViewById(R.id.button_create_crumb);

        this.factory = new UseCaseFactory(this);

        this.breadcrumbLocationListener = new BreadcrumbLocationListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_map_notes:
                Intent intent = new Intent(this, FullMapActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.menu_list_notes:
                Intent listIntent = new Intent(this, ListBreadcrumbsActivity.class);
                this.startActivity(listIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }

        if (!checkPermission()) {
            return;
        }

        this.breadcrumbLocationListener.setTempLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStop() {
        super.onStop();
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    @Override
    protected void onStart() {
        super.onStart();
        initLocationUpdates();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case TAG_CODE_PERMISSION_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.startLocationUpdates();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && requestCode == RESULT_CANCELED) {
            Log.d(CreateBreadcrumbActivity.class.getCanonicalName(), "Camera Intent Response: " + resultCode);
        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            galleryAddPic();
            setPic();
        }
    }

    private boolean checkPermission() {
        String[] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};

        if (!factory.newCheckPermissionsUseCase(permissions).execute()) {
            ActivityCompat.requestPermissions(this, permissions, TAG_CODE_PERMISSION_LOCATION);
            return false;
        }
        return true;
    }

    private void startLocationUpdates() {
        Log.d(CreateBreadcrumbActivity.class.getCanonicalName(), "init > startLocationUpdates");

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(ConstLocation.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(ConstLocation.FASTEST_INTERVAL);

        if (checkPermission()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this.breadcrumbLocationListener);
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, breadcrumbLocationListener);
        }
    }

    private void initLocationUpdates() {
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new BreadcrumbConnectionFailedListener())
                .build();

        mGoogleApiClient.connect();

        Log.d(CreateBreadcrumbActivity.class.getCanonicalName(), "init > mGoogleApiClient");
    }

    private String getCrumbText() {
        if (this.mEditText == null) {
            return null;
        }

        return mEditText.getText().toString();
    }

    private void processSuccess() {
        this.mEditText.setText(new String());

        FragmentManager fm = getSupportFragmentManager();
        ActionCompletedModal actionCompletedModal = ActionCompletedModal.newInstance();
        actionCompletedModal.show(fm, "fragment_edit_name");
    }

    public void saveNote(View view) {
        String note = getCrumbText();
        CrumbType crumbType = CrumbType.TEXT;

        if (note == null || note.isEmpty()) {
            Toast.makeText(this, R.string.error_input_missing_text, Toast.LENGTH_LONG).show();
            return;
        }

        Location mCurrentLocation = this.breadcrumbLocationListener.getLocation();

        this.breadCrumb = new BreadCrumb();
        this.breadCrumb.setTitle("Hello World");
        this.breadCrumb.setContent(note);

        if (this.mCurrentPhotoPath != null) {
            crumbType = CrumbType.IMAGE;
            this.breadCrumb.setMediaPath(this.mCurrentPhotoPath);
        }

        if (this.parentId != null) {
            this.breadCrumb.setParent(parentId);
        }

        if (mCurrentLocation != null) {
            this.breadCrumb.setLoc(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }

        this.breadCrumb.setType(crumbType);

        this.breadCrumb = factory.newSaveBreadcrumbUseCase(this.breadCrumb).execute();

        if (this.breadCrumb != null) {
            processSuccess();
        }
    }

    public void takePicture(View view) {
        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {

        File photoFile = factory.newCreateImageFileUseCase().execute();
        this.mCurrentPhotoPath = photoFile.getAbsolutePath();

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Check to see if there is an app installed to take a picture
        if (takePictureIntent.resolveActivity(getPackageManager()) == null) {
            return;
        }

        // Continue only if the File was successfully created
        if (photoFile == null) {
            return;
        }

        Uri photoURI = FileProvider.getUriForFile(this, "com.confusedpenguins.breadcrumbs.fileprovider", photoFile);

        Log.i(CreateBreadcrumbActivity.class.getCanonicalName(), photoURI.toString());

        takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }

    private void galleryAddPic() {
        if (this.mCurrentPhotoPath == null) {
            return;
        }

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();


        RoundedBitmapDrawable roundedBitmapDrawable = factory.newLoadRoundedBitmapDrawableUseCase(targetH, targetW, this.mCurrentPhotoPath).execute();
        mImageView.setImageDrawable(roundedBitmapDrawable);
    }

    public void listCrumbs(View view) {
        Intent intent = new Intent(this, ListBreadcrumbsActivity.class);
        this.startActivity(intent);
    }
}
