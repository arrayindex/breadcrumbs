package com.confusedpenguins.breadcrumbs.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.adapter.BreadcrumbAdapter;
import com.confusedpenguins.breadcrumbs.adapter.ClickListener;
import com.confusedpenguins.breadcrumbs.adapter.CrumbManager;
import com.confusedpenguins.breadcrumbs.adapter.RecyclerTouchListener;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.usecase.LoadSingleBreadcrumbUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Andrew on 8/27/2017.
 */

public class ViewBreadcrumbActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, CrumbManager {
    public static final String INTENT_BREADCRUMB = "BREADCRUMB";

    UseCaseFactory factory;
    RecyclerView mRecyclerView;
    private BreadcrumbAdapter mAdapter;
    private BreadCrumb breadCrumb;
    private GoogleMap mMap;
    private HashMap<String, Marker> markers = new HashMap<>();
    LoadSingleBreadcrumbUseCase updateData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_breadcrumb);
        String crumbId = this.getIntent().getStringExtra(INTENT_BREADCRUMB);

        if (crumbId == null) {
            Log.w(ViewBreadcrumbActivity.class.getName(), "Breadcrumb: missing " + crumbId);
            exit();
        }

        this.factory = new UseCaseFactory(this);
        updateData = factory.newLoadSingleBreadcrumbUseCase(crumbId);

        init();
    }

    private void init() {
        this.breadCrumb = updateData.execute();

        if (this.breadCrumb == null) {
            exit();
            return;
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setupRecycler();
    }

    private void setupRecycler() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_layout_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        ArrayList<BreadCrumb> crumbs = new ArrayList<>();
        crumbs.add(this.breadCrumb);
        crumbs.addAll(this.breadCrumb.getAfterThoughts());

        mAdapter = new BreadcrumbAdapter(factory);
        mAdapter.setBreadCrumbs(crumbs);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                BreadCrumb breadCrumb = mAdapter.getBreadcrumb(position);
                if (breadCrumb != null) {
                    ViewBreadcrumbActivity.this.focusMap(breadCrumb);
                    ViewBreadcrumbActivity.this.showBreadcrumbText(breadCrumb);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                BreadCrumb breadCrumb = mAdapter.getBreadcrumb(position);

                // Shouldn't happen
                if (breadCrumb == null) {
                    return;
                }

                // Ignore the parent Crumb
                if (breadCrumb.getParent() == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(ViewBreadcrumbActivity.this);
                builder.setTitle(breadCrumb.getContent());
                builder.setMessage(R.string.activity_list_alert_delete_crumb);
                builder.setIcon(R.drawable.ic_archive_black_24dp);

                builder.setPositiveButton(R.string.activity_list_alert_confirm, (dialog, id) -> {
                    boolean completed = factory.newArchiveBreadcrumbUseCase(breadCrumb).execute();
                    if (completed) {
                        mAdapter.remove(position);
                        mAdapter.notifyItemRemoved(position);
                        removeMarker(breadCrumb);
                    }
                });

                builder.setNegativeButton(R.string.activity_list_alert_cancel, (dialog, id) -> {

                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }));
    }

    private void exit() {
        try {
            this.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createMarker(BreadCrumb breadCrumb) {
        if (mMap != null && breadCrumb != null) {
            LatLng location = new LatLng(breadCrumb.getLatitude(), breadCrumb.getLongitude());
            Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(breadCrumb.getContent()));
            this.markers.put(breadCrumb.getAndroidId(), marker);
        }
    }

    private void removeMarker(BreadCrumb breadCrumb) {
        if (breadCrumb != null) {
            this.markers.get(breadCrumb.getAndroidId()).remove();
        }
    }

    public void createCrumb(View view) {
        FragmentManager fm = getSupportFragmentManager();
        CreateCrumbActivity editNameDialogFragment = CreateCrumbActivity.newInstance(CrumbType.TEXT, this.breadCrumb.getAndroidId());
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    private void focusMap(BreadCrumb breadCrumb) {
        if (mMap == null) {
            return;
        }

        LatLng location = new LatLng(breadCrumb.getLatitude(), breadCrumb.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private boolean showBreadcrumbText(BreadCrumb breadCrumb) {
        if (this.markers == null) {
            return false;
        }

        if (breadCrumb == null) {
            return false;
        }

        if (!this.markers.containsKey(breadCrumb.getAndroidId())) {
            return false;
        }

        this.markers.get(breadCrumb.getAndroidId()).showInfoWindow();

        return true;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);

        if (this.breadCrumb != null) {
            this.focusMap(this.breadCrumb);
            this.createMarker(this.breadCrumb);
            for (BreadCrumb crumb : this.breadCrumb.getAfterThoughts()) {
                this.createMarker(crumb);
            }
        }
    }


    private void addCrumb(BreadCrumb breadCrumb) {
        if (breadCrumb == null) {
            return;
        }

        this.mAdapter.add(breadCrumb);
        this.mAdapter.notifyItemInserted(0);
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        layoutManager.scrollToPositionWithOffset(0, 0);

        createMarker(breadCrumb);
        focusMap(breadCrumb);
        showBreadcrumbText(breadCrumb);
    }

    @Override
    public void onCreateBreadcrumb(BreadCrumb breadCrumb) {
        addCrumb(breadCrumb);
    }
}
