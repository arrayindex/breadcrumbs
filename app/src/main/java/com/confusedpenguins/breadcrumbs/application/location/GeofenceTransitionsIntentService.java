package com.confusedpenguins.breadcrumbs.application.location;

import static com.confusedpenguins.breadcrumbs.application.constants.ConstLocation.CONNECTION_TIME_OUT_MS;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.confusedpenguins.breadcrumbs.activity.ListBreadcrumbsActivity;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import com.google.android.gms.wearable.Wearable;

import java.util.concurrent.TimeUnit;


public class GeofenceTransitionsIntentService extends IntentService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;

    public GeofenceTransitionsIntentService() {
        super(GeofenceTransitionsIntentService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(GeofenceTransitionsIntentService.class.getCanonicalName(), "onCreate BreadcrumbsService Created");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /**
     * Handles incoming intents.
     *
     * @param intent The Intent sent by Location Services. This Intent is provided to Location
     *               Services (inside a PendingIntent) when addGeofences() is called.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geoFenceEvent = GeofencingEvent.fromIntent(intent);
        if (geoFenceEvent.hasError()) {
            int errorCode = geoFenceEvent.getErrorCode();
            Log.e(GeofenceTransitionsIntentService.class.getCanonicalName(), "Location Services error: " + errorCode);
        } else {

            int transitionType = geoFenceEvent.getGeofenceTransition();
            if (Geofence.GEOFENCE_TRANSITION_ENTER == transitionType) {
                // Connect to the Google Api service in preparation for sending a DataItem.

                mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                // Get the geofence id triggered. Note that only one geofence can be triggered at a
                // time in this example, but in some cases you might want to consider the full list
                // of geofences triggered.

                String triggeredGeoFenceId = geoFenceEvent.getTriggeringGeofences().get(0).getRequestId();
                executeBreadcrumbsIntent(triggeredGeoFenceId);
                mGoogleApiClient.disconnect();
            }
        }
    }

    private void executeBreadcrumbsIntent(String triggeredGeoFenceId) {
        UseCaseFactory factory = new UseCaseFactory(this);
        BreadCrumb breadCrumb = factory.newLoadSingleBreadcrumbUseCase(triggeredGeoFenceId).execute();
        factory.newCreateNotificationUseCase(breadCrumb, createPendingIntent()).execute();
    }

    private PendingIntent createPendingIntent() {
        Intent resultIntent = new Intent(this, ListBreadcrumbsActivity.class);
        return PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(GeofenceTransitionsIntentService.class.getCanonicalName(), "onConnected BreadcrumbsService Started");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(GeofenceTransitionsIntentService.class.getCanonicalName(), "onConnectionSuspended BreadcrumbsService Suspended: " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e(GeofenceTransitionsIntentService.class.getCanonicalName(), "onConnectionFailed BreadcrumbsService Error: " + result.getErrorMessage());
    }

}