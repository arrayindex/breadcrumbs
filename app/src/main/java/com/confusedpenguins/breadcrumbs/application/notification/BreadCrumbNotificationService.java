package com.confusedpenguins.breadcrumbs.application.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;


import com.confusedpenguins.breadcrumbs.R;

import java.util.Random;

import static android.content.Context.NOTIFICATION_SERVICE;


/**
 * Created by andrew on 1/9/18.
 */

public class BreadCrumbNotificationService {
    private Context context;

    public BreadCrumbNotificationService(Context context) {
        this.context = context;
    }

    public void postNotification(int title, String content, PendingIntent pendingIntent) {
        long[] vibratePattern = {25, 25, 25, 25, 25, 25};
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_room)
                        .setContentTitle(this.context.getString(title))
                        .setVibrate(vibratePattern)
                        .setLights(Color.CYAN, 1, 1)
                        .setContentText(content)
                        .setContentIntent(pendingIntent);


        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(new Random().nextInt(), mBuilder.build());
    }
}
