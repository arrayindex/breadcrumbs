package com.confusedpenguins.breadcrumbs.application.constants;

import android.content.Context;

import com.confusedpenguins.breadcrumbs.R;

import java.util.UUID;

/**
 * Created by Andrew on 5/2/2017.
 */

public class ApplicationSettings {

    private static String getApplicationApiRoot(Context context) {
        if (context != null) {
            return context.getString(R.string.api_home);
        }
        return null;
    }

    public static String getBreadcrumbsApi(Context context) {
        if (context != null) {
            return getApplicationApiRoot(context) + context.getString(R.string.api_breadcrumbs);
        }
        return null;
    }

    public static UUID getAndrioidUUID(Context context) {
        return new DeviceUuidFactory(context.getApplicationContext()).getDeviceUuid();
    }
}
