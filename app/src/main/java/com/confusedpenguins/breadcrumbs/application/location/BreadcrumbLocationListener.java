package com.confusedpenguins.breadcrumbs.application.location;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationListener;

/**
 * Created by Andrew on 5/27/2017.
 */

public class BreadcrumbLocationListener implements LocationListener {
    Location mCurrentLocation;

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
    }

    public Location getLocation() {
        return mCurrentLocation;
    }

    public void setTempLocation(Location location) {
        if (location != null) {
            this.mCurrentLocation = location;
        }
    }
}
