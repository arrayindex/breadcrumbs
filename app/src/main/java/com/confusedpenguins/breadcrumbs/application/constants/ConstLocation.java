package com.confusedpenguins.breadcrumbs.application.constants;

import android.text.format.DateUtils;

/**
 * Created by Andrew on 4/9/2017.
 */

public class ConstLocation {
    //GPS Location Tracker Constants
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    public static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    public static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    //Geofences
    public static final long GEOFENCE_RADIUS_IN_METERS = 100;
    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 24;
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * DateUtils.HOUR_IN_MILLIS;

    public static final long CONNECTION_TIME_OUT_MS = 20000;
    public static final String KEY_GEOFENCE_ID = "KEY_GEOFENCE_ID";
}
