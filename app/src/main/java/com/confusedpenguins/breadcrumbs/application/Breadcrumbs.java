package com.confusedpenguins.breadcrumbs.application;

import android.app.Application;
import android.content.res.Configuration;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by andrew on 12/31/17.
 */

public class Breadcrumbs extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        Log.i(Breadcrumbs.class.getCanonicalName(), "Application Started");
        Fabric.with(this, new Crashlytics());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
