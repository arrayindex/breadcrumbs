package com.confusedpenguins.breadcrumbs.application.location;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by Andrew on 5/27/2017.
 */

public class BreadcrumbConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(BreadcrumbConnectionFailedListener.class.getName(), "Connection Failed to Google API Client");
        Log.e(BreadcrumbConnectionFailedListener.class.getName(), "Error:" + connectionResult.getErrorMessage());
    }
}
