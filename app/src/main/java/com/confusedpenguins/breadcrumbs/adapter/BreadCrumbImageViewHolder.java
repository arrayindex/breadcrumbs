package com.confusedpenguins.breadcrumbs.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.confusedpenguins.breadcrumbs.R;

/**
 * Created by andrew on 1/18/18.
 */

public class BreadCrumbImageViewHolder extends RecyclerView.ViewHolder {
    TextView content;
    TextView date;
    ImageView imageView;

    public BreadCrumbImageViewHolder(View itemView) {
        super(itemView);

        this.content = (TextView) itemView.findViewById(R.id.breadcrumb_content);
        this.date = (TextView) itemView.findViewById(R.id.breadcrumb_date);
        this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
    }
}
