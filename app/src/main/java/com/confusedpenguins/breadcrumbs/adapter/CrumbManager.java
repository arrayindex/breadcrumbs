package com.confusedpenguins.breadcrumbs.adapter;

import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;

/**
 * Created by andrew on 1/6/18.
 */
public interface CrumbManager {
    void onCreateBreadcrumb(BreadCrumb breadCrumb);
}
