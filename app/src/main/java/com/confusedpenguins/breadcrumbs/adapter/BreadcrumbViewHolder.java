package com.confusedpenguins.breadcrumbs.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.confusedpenguins.breadcrumbs.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

/**
 * Created by Andrew on 8/27/2017.
 */

public class BreadcrumbViewHolder extends RecyclerView.ViewHolder{
    TextView content;
    TextView date;
    MaterialLetterIcon icon;

    public BreadcrumbViewHolder(View itemView) {
        super(itemView);

        this.content = (TextView) itemView.findViewById(R.id.breadcrumb_content);
        this.date = (TextView) itemView.findViewById(R.id.breadcrumb_date);
        this.icon = (MaterialLetterIcon) itemView.findViewById(R.id.breadcrumb_icon);
    }

}