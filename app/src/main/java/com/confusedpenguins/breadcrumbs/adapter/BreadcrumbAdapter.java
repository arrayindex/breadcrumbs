package com.confusedpenguins.breadcrumbs.adapter;

import android.content.Context;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCaseFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Andrew on 8/27/2017.
 */

public class BreadcrumbAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TEXT_CRUMB = 0;
    private static final int IMAGE_CRUMB = 1;

    private UseCaseFactory factory;
    private ArrayList<BreadCrumb> breadCrumbs = null;

    public BreadcrumbAdapter(UseCaseFactory factory) {
        this.factory = factory;
    }

    public void setBreadCrumbs(ArrayList<BreadCrumb> breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
        sortCrumbs();
    }

    public void add(BreadCrumb breadCrumb) {
        if (breadCrumb != null) {
            this.breadCrumbs.add(breadCrumb);
            sortCrumbs();
        }
    }

    public void remove(int position) {
        if (this.breadCrumbs == null) {
            return;
        }

        if (position < 0) {
            return;
        }

        if (this.breadCrumbs.size() < position) {
            return;
        }

        this.breadCrumbs.remove(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        switch (viewType) {
            case TEXT_CRUMB:
                View textCrumb = LayoutInflater.from(context).inflate(R.layout.item_breadcrumb, parent, false);
                return new BreadcrumbViewHolder(textCrumb);
            case IMAGE_CRUMB:
                View imageCrumb = LayoutInflater.from(context).inflate(R.layout.item_breadcrumb_image, parent, false);
                return new BreadCrumbImageViewHolder(imageCrumb);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TEXT_CRUMB: {
                renderTextCard((BreadcrumbViewHolder) holder, position);
                break;
            }
            case IMAGE_CRUMB: {
                renderImageCard((BreadCrumbImageViewHolder) holder, position);
                break;
            }
        }
    }

    private void renderImageCard(final BreadCrumbImageViewHolder holder, int position) {
        if (holder == null) {
            return;
        }

        String dateFormat = "MMM-dd-yyyy HH:mm";
        BreadCrumb tmp = this.breadCrumbs.get(position);

        if (tmp == null) {
            return;
        }

        int targetW = 300;
        int targetH = 300;

        Log.i(BreadcrumbAdapter.class.getCanonicalName(), "Width: " + targetW);
        Log.i(BreadcrumbAdapter.class.getCanonicalName(), "Height: " + targetH);

        RoundedBitmapDrawable roundedBitmapDrawable = factory.newLoadRoundedBitmapDrawableUseCase(targetH, targetW, tmp.getMediaPath()).execute();

        holder.content.setText(tmp.getContent());
        holder.imageView.setImageDrawable(roundedBitmapDrawable);

        SimpleDateFormat iso8601Format = new SimpleDateFormat(dateFormat);
        holder.date.setText(iso8601Format.format(tmp.getCreated()));
    }

    private void renderTextCard(final BreadcrumbViewHolder holder, int position) {
        if (holder == null) {
            return;
        }

        String dateFormat = "MMM-dd-yyyy HH:mm";
        BreadCrumb tmp = this.breadCrumbs.get(position);

        if (tmp == null) {
            return;
        }

        holder.content.setText(tmp.getContent().trim());
        holder.icon.setLetter(tmp.getContent().charAt(0) + "");

        if (!Character.isLetter(tmp.getContent().charAt(0))) {
            holder.icon.setLetter("@");
        }

        SimpleDateFormat iso8601Format = new SimpleDateFormat(dateFormat);
        holder.date.setText(iso8601Format.format(tmp.getCreated()));
    }

    @Override
    public int getItemCount() {
        return (breadCrumbs != null) ? breadCrumbs.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.breadCrumbs == null) {
            return -1;
        }

        if (this.breadCrumbs.isEmpty()) {
            return -1;
        }

        if (CrumbType.TEXT.equals(this.breadCrumbs.get(position).getType())) {
            return TEXT_CRUMB;
        }

        if (CrumbType.IMAGE.equals(this.breadCrumbs.get(position).getType())) {
            return IMAGE_CRUMB;
        }

        return -1;
    }

    public BreadCrumb getBreadcrumb(int position) {
        if (position < 0) {
            return null;
        }

        if (this.breadCrumbs == null) {
            return null;
        }

        if (position > this.breadCrumbs.size()) {
            return null;
        }

        return this.breadCrumbs.get(position);
    }

    private void sortCrumbs() {
        if (this.breadCrumbs == null) {
            return;
        }
        Collections.sort(this.breadCrumbs, (p1, p2) -> p2.getCreated().compareTo(p1.getCreated()));
    }
}
