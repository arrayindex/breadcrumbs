package com.confusedpenguins.breadcrumbs.adapter;

import android.view.View;

/**
 * Created by andrew on 1/1/18.
 */

public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
