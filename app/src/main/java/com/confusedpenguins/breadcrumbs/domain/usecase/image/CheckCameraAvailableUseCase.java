package com.confusedpenguins.breadcrumbs.domain.usecase.image;

import android.content.pm.PackageManager;

import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;

/**
 * Created by andrew on 1/15/18.
 */

public class CheckCameraAvailableUseCase implements UseCase<Boolean> {
    private PackageManager packageManager;


    public CheckCameraAvailableUseCase(PackageManager packageManager) {
        this.packageManager = packageManager;
    }

    @Override
    public Boolean execute() {
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
}
