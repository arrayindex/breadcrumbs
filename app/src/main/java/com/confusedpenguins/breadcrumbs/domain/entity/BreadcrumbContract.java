package com.confusedpenguins.breadcrumbs.domain.entity;

import android.provider.BaseColumns;

/**
 * Created by Andrew on 3/28/2017.
 */

public class BreadcrumbContract {

    private BreadcrumbContract() {
    }

    /* Inner class that defines the table contents */
    public static class BreadcrumbEntry implements BaseColumns {
        public static final String TABLE_NAME = "breadcrumb";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_CREATED = "created";
        public static final String COLUMN_NAME_SAVED = "saved";
        public static final String COLUMN_NAME_VISIBLE = "visible";
        public static final String COLUMN_NAME_PARENT = "parent";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_MEDIA_URI = "media";
    }
}
