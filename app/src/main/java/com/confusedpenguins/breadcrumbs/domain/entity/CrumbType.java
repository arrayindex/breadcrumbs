package com.confusedpenguins.breadcrumbs.domain.entity;

/**
 * Created by andrew on 1/16/18.
 */

public enum CrumbType {
    TEXT, IMAGE, VIDEO,;

    public static CrumbType get(String type) {
        if (type == null) {
            return TEXT;
        }

        return valueOf(type);
    }
}
