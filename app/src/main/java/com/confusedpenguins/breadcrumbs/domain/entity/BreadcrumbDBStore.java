package com.confusedpenguins.breadcrumbs.domain.entity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Andrew on 3/28/2017.
 */

public class BreadcrumbDBStore extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "FeedReader.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BreadcrumbContract.BreadcrumbEntry.TABLE_NAME + " (" +
                    BreadcrumbContract.BreadcrumbEntry._ID + " INTEGER PRIMARY KEY," +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TITLE + " TEXT," +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CONTENT + " TEXT," +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LATITUDE + " INTEGER," +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LONGITUDE + " INTEGER, " +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CREATED + " TEXT," +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_SAVED + " INTEGER, " +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT + " INTEGER, " +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE + " TEXT, " +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI + " TEXT, " +
                    BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE + " INTEGER" +
                    ")";

    public BreadcrumbDBStore(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.i(BreadcrumbDBStore.class.getName(), SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(BreadcrumbDBStore.class.getName(), "Old Version: " + oldVersion);
        Log.i(BreadcrumbDBStore.class.getName(), "New Version: " + newVersion);
        switch (oldVersion) {
            case 3:
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_SAVED, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI, "TEXT");
                break;
            case 4:
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI, "TEXT");
                break;
            case 5:
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI, "TEXT");
                break;
            case 6:
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE, "TEXT");
                addColumn(db, BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI, "TEXT");
            default:
                break;
        }
        //onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //onUpgrade(db, oldVersion, newVersion);
        Log.i(BreadcrumbDBStore.class.getCanonicalName(), "On Downgrade: " + newVersion);
    }

    private boolean addColumn(SQLiteDatabase db, String columnName, String type) {
        if (!existsColumnInTable(db, BreadcrumbContract.BreadcrumbEntry.TABLE_NAME, columnName)) {
            Log.i(BreadcrumbDBStore.class.getCanonicalName(), "On addColumn: " + columnName);
            String alterSQL = String.format("ALTER TABLE %s ADD COLUMN %s %s;", BreadcrumbContract.BreadcrumbEntry.TABLE_NAME, columnName, type);
            Log.i(BreadcrumbDBStore.class.getCanonicalName(), alterSQL);
            db.execSQL(alterSQL);
        }
        return true;
    }

    private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        Log.d(BreadcrumbDBStore.class.getName(), "existsColumnInTable: " + inTable + " columnToCheck:" + columnToCheck);
        Cursor mCursor = null;
        try {
            // Query 1 row
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);
            // getColumnIndex() gives us the index (0 to ...) of the column - otherwise we get a -1
            if (mCursor.getColumnIndex(columnToCheck) != -1) {
                return true;
            } else {
                return false;
            }

        } catch (Exception Exp) {
            Log.d(BreadcrumbDBStore.class.getName(), "An error occurred: " + Exp.getMessage());
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }
}
