package com.confusedpenguins.breadcrumbs.domain.location;

import android.app.PendingIntent;

import com.confusedpenguins.breadcrumbs.domain.PutBundle;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

/**
 * Created by andrew on 1/9/18.
 */

public class GeoFencePutBundle extends PutBundle {
    private GeofencingRequest geofencingRequest;
    private PendingIntent pendingIntent;

    private OnSuccessListener onSuccessListener;
    private OnFailureListener onFailureListener;

    public GeoFencePutBundle(GeofencingRequest geofencingRequest, PendingIntent pendingIntent, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        this.geofencingRequest = geofencingRequest;
        this.pendingIntent = pendingIntent;
        this.onSuccessListener = onSuccessListener;
        this.onFailureListener = onFailureListener;
    }

    public GeofencingRequest getGeofencingRequest() {
        return geofencingRequest;
    }

    public PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public OnSuccessListener getOnSuccessListener() {
        return onSuccessListener;
    }

    public OnFailureListener getOnFailureListener() {
        return onFailureListener;
    }

    @Override
    public boolean validate() {
        if (this.geofencingRequest == null) {
            return false;
        }

        if (this.pendingIntent == null) {
            return false;
        }

        if (this.onSuccessListener == null) {
            return false;
        }

        if (this.onFailureListener == null) {
            return false;
        }

        return true;
    }
}
