package com.confusedpenguins.breadcrumbs.domain.entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Andrew on 4/9/2017.
 */

public class BreadCrumb implements Serializable {
    private String title;
    private String content;
    private String androidId;
    private Loc loc;
    private Date created;
    private boolean saved;
    private boolean visible;
    private String parent;
    private String mediaPath;
    private ArrayList<BreadCrumb> afterThoughts;

    private CrumbType type;

    public void addAfterThought(BreadCrumb breadCrumb) {
        if (this.afterThoughts == null) {
            this.afterThoughts = new ArrayList<>();
        }
        this.afterThoughts.add(breadCrumb);
    }

    public ArrayList<BreadCrumb> getAfterThoughts() {
        return (afterThoughts != null) ? this.afterThoughts : new ArrayList<BreadCrumb>();
    }

    public void setAfterThoughts(ArrayList<BreadCrumb> afterThoughts) {
        this.afterThoughts = afterThoughts;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public boolean hasParent() {
        return (this.parent == null) ? false : true;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = (null == visible || "1".equals(visible));
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setLoc(double latitude, double longitude) {
        double[] tmp = {latitude, longitude};
        Loc location = new Loc(tmp);
        this.loc = location;
    }

    public double getLatitude() {
        return (this.loc != null) ? this.loc.getLatitude() : 0.0;
    }

    public double getLongitude() {
        return (this.loc != null) ? this.loc.getLongitude() : 0.0;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public CrumbType getType() {
        return (type == null) ? CrumbType.TEXT : type;
    }

    public void setType(CrumbType type) {
        this.type = type;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    @Override
    public String toString() {
        return "BreadCrumb{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", androidId='" + androidId + '\'' +
                ", loc=" + loc +
                ", created=" + created +
                ", saved=" + saved +
                ", visible=" + visible +
                ", parent='" + parent + '\'' +
                ", afterThoughts=" + afterThoughts +
                '}';
    }

    public JSONObject toJSONObject() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("title", this.title);
            payload.put("content", this.content);
            payload.put("loc", this.loc.toJSONObject());
            payload.put("androidId", this.androidId);
            payload.put("created", this.created);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    public boolean validateLocation() {
        if (this.loc == null) {
            return false;
        }

        return this.loc.validate();
    }
}
