package com.confusedpenguins.breadcrumbs.domain.usecase;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import java.util.List;

/**
 * Created by andrew on 1/18/18.
 */

public class CheckPermissionsUseCase implements UseCase<Boolean> {
    String[] requestedPermissions;
    Context context;

    public CheckPermissionsUseCase(String[] requestedPermissions, Context context) {
        this.requestedPermissions = requestedPermissions;
        this.context = context;
    }

    @Override
    public Boolean execute() {
        if (context == null) {
            return false;
        }

        if (requestedPermissions == null) {
            return false;
        }

        if (requestedPermissions.length == 0) {
            return true;
        }

        for (String permission : this.requestedPermissions) {
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(this.context, permission)) {
                return false;
            }
        }

        return true;
    }
}
