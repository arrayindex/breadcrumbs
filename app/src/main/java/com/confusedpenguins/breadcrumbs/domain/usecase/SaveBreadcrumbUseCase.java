package com.confusedpenguins.breadcrumbs.domain.usecase;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.application.Breadcrumbs;
import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumbPutBundle;
import com.confusedpenguins.breadcrumbs.domain.entity.CrumbType;
import com.confusedpenguins.breadcrumbs.domain.metric.MetricLogger;

/**
 * Created by andrew on 12/30/17.
 */

public class SaveBreadcrumbUseCase implements UseCase<BreadCrumb> {
    Repository<BreadCrumb, BreadCrumbPutBundle> entityController;
    private BreadCrumb breadCrumb;
    private MetricLogger metricLogger;

    protected SaveBreadcrumbUseCase(Repository<BreadCrumb, BreadCrumbPutBundle> entityController, BreadCrumb breadCrumb, MetricLogger metricLogger) {
        this.entityController = entityController;
        this.breadCrumb = breadCrumb;
        this.metricLogger = metricLogger;
    }

    private boolean logMetric() {
        int tag = R.string.metric_create_crumb;

        if (this.breadCrumb.hasParent()) {
            tag = R.string.metric_create_afterthought;
        }

        if (CrumbType.IMAGE.equals(this.breadCrumb.getType())) {
            tag = R.string.metric_create_image;
        }

        return this.metricLogger.logMetric(tag);
    }

    @Override
    public BreadCrumb execute() {
        try {
            if (breadCrumb == null) {
                return null;
            }

            this.breadCrumb = this.entityController.put(new BreadCrumbPutBundle(breadCrumb));

            if (this.breadCrumb == null) {
                return null;
            }

            if (metricLogger != null) {
                logMetric();
            }

            return this.breadCrumb;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
