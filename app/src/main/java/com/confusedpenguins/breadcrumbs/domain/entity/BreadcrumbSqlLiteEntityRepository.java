package com.confusedpenguins.breadcrumbs.domain.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.confusedpenguins.breadcrumbs.domain.PutBundle;
import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.exception.BreadCrumbException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Andrew on 4/9/2017.
 */

public class BreadcrumbSqlLiteEntityRepository implements Repository<BreadCrumb, BreadCrumbPutBundle> {
    public static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
    private BreadcrumbDBStore breadcrumbDBStore;

    public BreadcrumbSqlLiteEntityRepository(BreadcrumbDBStore breadcrumbDBStore) {
        this.breadcrumbDBStore = breadcrumbDBStore;
    }

    String[] columns = {
            BreadcrumbContract.BreadcrumbEntry._ID,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TITLE,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CONTENT,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LATITUDE,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LONGITUDE,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CREATED,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_SAVED,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE,
            BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI,
    };

    @Override
    public ArrayList<BreadCrumb> getAll() {
        SQLiteDatabase db = breadcrumbDBStore.getReadableDatabase();

        String selection = String.format("(%s is null or %s = 1)",
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE);

        Cursor resultSet = db.query(BreadcrumbContract.BreadcrumbEntry.TABLE_NAME,
                columns,
                selection,
                null,
                null,
                null,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CREATED + " desc"
        );

        HashMap<String, BreadCrumb> breadCrumbsMap = processResultSet(resultSet);
        ArrayList<BreadCrumb> breadCrumbs = new ArrayList<>();

        for (String key : breadCrumbsMap.keySet()) {
            breadCrumbs.add(breadCrumbsMap.get(key));
        }

        db.close();
        return breadCrumbs;
    }

    @Override
    public BreadCrumb get(String id) {
        SQLiteDatabase db = breadcrumbDBStore.getReadableDatabase();
        String[] selectionArgs = {id, id};

        String where = String.format("(%s = ? or %s = ?) and (%s is null or %s = 1)",
                BreadcrumbContract.BreadcrumbEntry._ID,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE);

        Cursor resultSet = db.query(BreadcrumbContract.BreadcrumbEntry.TABLE_NAME,
                columns,
                where,
                selectionArgs,
                null,
                null,
                null);

        BreadCrumb breadCrumb = processResultSet(resultSet).get(id);
        db.close();
        return breadCrumb;
    }

    @Override
    public ArrayList<BreadCrumb> query(String query) {
        if (query == null) {
            return new ArrayList<>();
        }

        SQLiteDatabase db = breadcrumbDBStore.getReadableDatabase();

        String[] selectionArgs = {"%" + query + "%"};

        String where = String.format("(%s LIKE ?) and (%s is null or %s = 1)",
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CONTENT,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE,
                BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE);

        Cursor resultSet = db.query(BreadcrumbContract.BreadcrumbEntry.TABLE_NAME,
                columns,
                where,
                selectionArgs,
                null,
                null,
                null);

        HashMap<String, BreadCrumb> breadCrumbsMap = processResultSet(resultSet);
        ArrayList<BreadCrumb> breadCrumbs = new ArrayList<>();
        for (String key : breadCrumbsMap.keySet()) {
            breadCrumbs.add(breadCrumbsMap.get(key));
        }
        db.close();
        return breadCrumbs;
    }

    @Override
    public boolean unArchive(BreadCrumb breadCrumb) {
        if (breadCrumb == null) {
            return false;
        }

        SQLiteDatabase db = breadcrumbDBStore.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE, "1");
        db.update(
                BreadcrumbContract.BreadcrumbEntry.TABLE_NAME, args,
                String.format("%s = ?", BreadcrumbContract.BreadcrumbEntry._ID),
                new String[]{breadCrumb.getAndroidId()});
        db.close();
        return true;

    }

    @Override
    public boolean archive(BreadCrumb breadCrumb) {
        if (breadCrumb == null) {
            return false;
        }

        SQLiteDatabase db = breadcrumbDBStore.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE, "0");

        db.update(BreadcrumbContract.BreadcrumbEntry.TABLE_NAME,
                args, String.format("%s = ?", BreadcrumbContract.BreadcrumbEntry._ID),
                new String[]{breadCrumb.getAndroidId()}
        );
        db.close();

        return true;
    }

    @Override
    public BreadCrumb put(BreadCrumbPutBundle breadCrumbPutBundle) {
        if (breadCrumbPutBundle == null || !breadCrumbPutBundle.validate()) {
            return null;
        }

        BreadCrumb breadCrumb = breadCrumbPutBundle.getBreadCrumb();
        SimpleDateFormat iso8601Format = new SimpleDateFormat(dateFormat);
        Date date = new Date();

        ContentValues values = new ContentValues();
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TITLE, breadCrumb.getTitle());
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CONTENT, breadCrumb.getContent());
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LATITUDE, breadCrumb.getLatitude());
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LONGITUDE, breadCrumb.getLongitude());
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CREATED, iso8601Format.format(date));
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE, "1");
        values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE, breadCrumb.getType().name());

        if (breadCrumb.getParent() != null) {
            values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT, breadCrumb.getParent());
        }

        if (breadCrumb.getMediaPath() != null) {
            values.put(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI, breadCrumb.getMediaPath());
        }

        SQLiteDatabase db = breadcrumbDBStore.getWritableDatabase();
        long newRowId = db.insert(BreadcrumbContract.BreadcrumbEntry.TABLE_NAME, null, values);
        db.close();

        if (newRowId == -1) {
            return null;
        }

        breadCrumb.setAndroidId(String.valueOf(newRowId));
        breadCrumb.setCreated(date);
        return breadCrumb;
    }// end method saveBreadcrumb

    @Override
    public BreadCrumb post(BreadCrumb breadCrumb) throws BreadCrumbException {
        throw new BreadCrumbException("Not Yet Implemented");
    }

    private HashMap<String, BreadCrumb> processResultSet(Cursor cursor) {
        HashMap<String, BreadCrumb> breadCrumbHashMap = new HashMap<>();
        ArrayList<BreadCrumb> children = new ArrayList<>();

        if (cursor == null || cursor.isClosed()) {
            return breadCrumbHashMap;
        }

        SimpleDateFormat iso8601Format = new SimpleDateFormat(dateFormat);
        while (cursor.moveToNext()) {
            BreadCrumb breadCrumb = new BreadCrumb();
            breadCrumb.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TITLE)));
            breadCrumb.setContent(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CONTENT)));
            breadCrumb.setLoc(
                    cursor.getDouble(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LATITUDE)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_LONGITUDE))
            );
            breadCrumb.setAndroidId(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry._ID)));
            breadCrumb.setSaved("true".equals(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_SAVED))));
            breadCrumb.setVisible(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_VISIBLE)));
            breadCrumb.setParent(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_PARENT)));

            breadCrumb.setType(CrumbType.get(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_TYPE))));
            breadCrumb.setMediaPath(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_MEDIA_URI)));

            try {
                breadCrumb.setCreated(iso8601Format.parse(cursor.getString(cursor.getColumnIndexOrThrow(BreadcrumbContract.BreadcrumbEntry.COLUMN_NAME_CREATED))));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (breadCrumb.hasParent()) {
                children.add(breadCrumb);
            } else {
                breadCrumbHashMap.put(breadCrumb.getAndroidId(), breadCrumb);
            }
        }

        for (BreadCrumb child : children) {
            if (breadCrumbHashMap.get(child.getParent()) != null) {
                breadCrumbHashMap.get(child.getParent()).addAfterThought(child);
            }
        }
        cursor.close();

        return breadCrumbHashMap;
    }
}
