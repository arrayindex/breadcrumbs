package com.confusedpenguins.breadcrumbs.domain.usecase;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumbPutBundle;

import java.util.ArrayList;

/**
 * Created by andrew on 12/30/17.
 */

public class LoadBreadcrumbsUseCase implements UseCase<ArrayList<BreadCrumb>> {
    private String query;
    Repository<BreadCrumb, BreadCrumbPutBundle> entityController;

    protected LoadBreadcrumbsUseCase(Repository<BreadCrumb, BreadCrumbPutBundle> entityController, String query) {
        this.entityController = entityController;
        this.query = query;
    }

    @Override
    public ArrayList<BreadCrumb> execute() {
        try {

            if (query != null && !query.isEmpty()) {
                return entityController.query(query);
            }

            return entityController.getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
