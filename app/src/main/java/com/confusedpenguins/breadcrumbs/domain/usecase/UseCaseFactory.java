package com.confusedpenguins.breadcrumbs.domain.usecase;

import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.application.notification.BreadCrumbNotificationService;
import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumbPutBundle;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadcrumbDBStore;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadcrumbSqlLiteEntityRepository;
import com.confusedpenguins.breadcrumbs.domain.location.GeoFencePutBundle;
import com.confusedpenguins.breadcrumbs.domain.location.GeoFenceRepository;
import com.confusedpenguins.breadcrumbs.domain.metric.FabricMetricLogger;
import com.confusedpenguins.breadcrumbs.domain.metric.MetricLogger;
import com.confusedpenguins.breadcrumbs.domain.usecase.image.CheckCameraAvailableUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.image.CreateImageFileUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.image.LoadBitmapUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.image.LoadRoundedBitmapDrawableUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.location.AddGeofencesUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.location.FindCenterPointUseCase;
import com.confusedpenguins.breadcrumbs.domain.usecase.notification.CreateNotificationUseCase;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 12/30/17.
 */

public class UseCaseFactory {
    private Context context;
    private BreadCrumbNotificationService breadCrumbNotificationService;

    private Repository<Geofence, GeoFencePutBundle> geoFenceRepository;
    private Repository<BreadCrumb, BreadCrumbPutBundle> breadcrumbEntityRepository;
    private MetricLogger metricLogger;
    private PackageManager packageManager;

    public UseCaseFactory(Context context) {
        this.context = context;

        BreadcrumbDBStore breadcrumbDBStore = new BreadcrumbDBStore(this.context);
        this.breadcrumbEntityRepository = new BreadcrumbSqlLiteEntityRepository(breadcrumbDBStore);

        GeofencingClient mGeoFencingClient = LocationServices.getGeofencingClient(this.context);
        this.geoFenceRepository = new GeoFenceRepository(mGeoFencingClient);


        this.breadCrumbNotificationService = new BreadCrumbNotificationService(this.context);
        this.metricLogger = new FabricMetricLogger(context);

        this.packageManager = context.getPackageManager();
    }

    public LoadBreadcrumbsUseCase newLoadBreadcrumbUseCase(String query) {
        return new LoadBreadcrumbsUseCase(breadcrumbEntityRepository, query);
    }

    public ArchiveBreadcrumbUseCase newArchiveBreadcrumbUseCase(BreadCrumb breadCrumb) {
        return new ArchiveBreadcrumbUseCase(breadcrumbEntityRepository, breadCrumb);
    }

    public LoadSingleBreadcrumbUseCase newLoadSingleBreadcrumbUseCase(String breadcrumbId) {
        return new LoadSingleBreadcrumbUseCase(breadcrumbEntityRepository, breadcrumbId);
    }

    public SaveBreadcrumbUseCase newSaveBreadcrumbUseCase(BreadCrumb breadCrumb) {
        return new SaveBreadcrumbUseCase(breadcrumbEntityRepository, breadCrumb, metricLogger);
    }

    public AddGeofencesUseCase newAddGeofencesUseCase(ArrayList<BreadCrumb> breadCrumbs, PendingIntent pendingIntent, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        return new AddGeofencesUseCase(this.geoFenceRepository, breadCrumbs, pendingIntent, onSuccessListener, onFailureListener);
    }

    public CreateNotificationUseCase newCreateNotificationUseCase(BreadCrumb breadCrumb, PendingIntent intent) {
        return new CreateNotificationUseCase(breadCrumbNotificationService, intent, breadCrumb);
    }

    public FindCenterPointUseCase newFindCenterPointUseCase(ArrayList<BreadCrumb> breadCrumbs) {
        return new FindCenterPointUseCase(breadCrumbs);
    }

    public CheckCameraAvailableUseCase newCheckCameraAvailableUseCase() {
        return new CheckCameraAvailableUseCase(this.packageManager);
    }

    public CreateImageFileUseCase newCreateImageFileUseCase() {
        String applicationName = context.getString(R.string.app_name);
        return new CreateImageFileUseCase(applicationName);
    }

    public LoadBitmapUseCase newLoadBitmapUseCase(String path) {
        return new LoadBitmapUseCase(path);
    }

    public LoadRoundedBitmapDrawableUseCase newLoadRoundedBitmapDrawableUseCase(int height, int width, String path) {
        return new LoadRoundedBitmapDrawableUseCase(context.getResources(), width, height, path);
    }

    public CheckPermissionsUseCase newCheckPermissionsUseCase(String[] requestedPermissions) {
        return new CheckPermissionsUseCase(requestedPermissions, context);
    }
}
