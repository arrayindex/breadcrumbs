package com.confusedpenguins.breadcrumbs.domain.usecase.location;

import android.util.Log;

import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;

import java.util.ArrayList;

import com.confusedpenguins.breadcrumbs.application.constants.ConstLocation;

/**
 * Created by andrew on 1/9/18.
 */

public class CreateGeofencingRequestUseCase implements UseCase<GeofencingRequest> {
    private ArrayList<BreadCrumb> breadCrumbs;

    protected CreateGeofencingRequestUseCase(ArrayList<BreadCrumb> breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
    }

    @Override
    public GeofencingRequest execute() {
        if (breadCrumbs == null || breadCrumbs.isEmpty()) {
            return null;
        }

        ArrayList<Geofence> mGeofenceList = new ArrayList<>();

        for (BreadCrumb breadCrumb : this.breadCrumbs) {
            mGeofenceList.add(createFence(breadCrumb));
        }

        return getGeofencingRequest(mGeofenceList);
    }

    private GeofencingRequest getGeofencingRequest(ArrayList<Geofence> mGeofenceList) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private Geofence createFence(BreadCrumb breadCrumb) {
        Log.i(CreateGeofencingRequestUseCase.class.getCanonicalName(), "Created Geo Fence for " + breadCrumb.getAndroidId());
        return new Geofence.Builder()
                .setRequestId(breadCrumb.getAndroidId())

                .setCircularRegion(
                        breadCrumb.getLatitude(),
                        breadCrumb.getLongitude(),
                        ConstLocation.GEOFENCE_RADIUS_IN_METERS
                )
                .setExpirationDuration(ConstLocation.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(1000)
                .build();
    }
}
