package com.confusedpenguins.breadcrumbs.domain.entity;

import com.confusedpenguins.breadcrumbs.domain.PutBundle;

/**
 * Created by andrew on 1/9/18.
 */

public class BreadCrumbPutBundle extends PutBundle {
    private BreadCrumb breadCrumb;

    public BreadCrumbPutBundle(BreadCrumb breadCrumb) {
        this.breadCrumb = breadCrumb;
    }

    public BreadCrumb getBreadCrumb() {
        return breadCrumb;
    }

    @Override
    public boolean validate() {
        if (breadCrumb == null) {
            return false;
        }

        return true;
    }
}
