package com.confusedpenguins.breadcrumbs.domain.usecase.location;

import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 1/15/18.
 */

public class FindCenterPointUseCase implements UseCase<LatLng> {
    private ArrayList<BreadCrumb> breadCrumbs;

    public FindCenterPointUseCase(ArrayList<BreadCrumb> breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
    }

    private LatLng computeCentroid(List<LatLng> points) {
        double latitude = 0;
        double longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.latitude;
            longitude += point.longitude;
        }

        return new LatLng(latitude / n, longitude / n);
    }

    private List<LatLng> exportPoints() {
        List<LatLng> points = new ArrayList<>();

        for (BreadCrumb crumb : this.breadCrumbs) {
            if (crumb.validateLocation()) {
                points.add(new LatLng(crumb.getLatitude(), crumb.getLongitude()));
            }
        }

        return points;
    }

    @Override
    public LatLng execute() {
        return computeCentroid(exportPoints());
    }
}
