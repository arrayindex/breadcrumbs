package com.confusedpenguins.breadcrumbs.domain.usecase;

import android.util.Log;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumbPutBundle;

/**
 * Created by andrew on 12/30/17.
 */

public class ArchiveBreadcrumbUseCase implements UseCase<Boolean> {
    Repository<BreadCrumb, BreadCrumbPutBundle> entityController;
    private BreadCrumb breadCrumb;

    protected ArchiveBreadcrumbUseCase(Repository<BreadCrumb, BreadCrumbPutBundle> entityController, BreadCrumb breadCrumb) {
        this.entityController = entityController;
        this.breadCrumb = breadCrumb;
    }

    @Override
    public Boolean execute() {
        if (this.breadCrumb == null) {
            Log.e(ArchiveBreadcrumbUseCase.class.getCanonicalName(), "Missing Crumb");
            return false;
        }
        try {
            return this.entityController.archive(breadCrumb);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
