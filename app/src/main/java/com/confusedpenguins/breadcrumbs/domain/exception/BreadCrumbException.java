package com.confusedpenguins.breadcrumbs.domain.exception;

/**
 * Created by andrew on 1/9/18.
 */

public class BreadCrumbException extends Exception {
    public BreadCrumbException(String message) {
        super(message);
    }
}
