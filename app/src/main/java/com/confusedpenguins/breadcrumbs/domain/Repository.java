package com.confusedpenguins.breadcrumbs.domain;

import java.util.ArrayList;

/**
 * Created by andrew on 1/9/18.
 */

/**
 * Repository Interface
 *
 * @param <E> Core Object to be stored
 * @param <T> Put Bundle for inserts
 */
public interface Repository<E, PutBundle> {
    ArrayList<E> getAll() throws Exception;

    E get(String id) throws Exception;

    ArrayList<E> query(String query) throws Exception;

    boolean archive(E query) throws Exception;

    boolean unArchive(E query) throws Exception;

    E put(PutBundle entity) throws Exception;

    E post(E entity) throws Exception;
}
