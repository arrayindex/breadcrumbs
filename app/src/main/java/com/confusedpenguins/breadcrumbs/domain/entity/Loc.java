package com.confusedpenguins.breadcrumbs.domain.entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Andrew on 5/1/2017.
 */

public class Loc implements Serializable {
    private String type;
    private double[] coordinates;

    public Loc(double[] coordinates) {
        this.type = "Point";
        this.coordinates = coordinates;
    }

    public double getLatitude() {
        return (this.coordinates != null && this.coordinates.length == 2) ? this.coordinates[0] : 0.0;
    }

    public double getLongitude() {
        return (this.coordinates != null && this.coordinates.length == 2) ? this.coordinates[1] : 0.0;
    }

    public boolean validate() {
        if (coordinates == null) {
            return false;
        }

        if (coordinates.length != 2) {
            return false;
        }

        if (coordinates[0] == 0) {
            return false;
        }

        if (coordinates[1] == 0) {
            return false;
        }

        return true;
    }

    public JSONObject toJSONObject() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("type", this.type);
            payload.put("coordinates", new JSONArray(this.coordinates));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    @Override
    public String toString() {
        return "Loc{" +
                "type='" + type + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                '}';
    }
}
