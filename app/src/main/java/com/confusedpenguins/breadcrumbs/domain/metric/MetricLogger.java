package com.confusedpenguins.breadcrumbs.domain.metric;

/**
 * Created by andrew on 1/7/18.
 */

public interface MetricLogger {
    boolean logMetric(int tag);
}
