package com.confusedpenguins.breadcrumbs.domain.location;

import android.app.Activity;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.exception.BreadCrumbException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;

import java.util.ArrayList;

/**
 * Created by andrew on 1/9/18.
 */

public class GeoFenceRepository implements Repository<Geofence, GeoFencePutBundle> {
    private GeofencingClient mGeofencingClient;

    public GeoFenceRepository(GeofencingClient mGeofencingClient) {
        this.mGeofencingClient = mGeofencingClient;
    }

    @Override
    public ArrayList<Geofence> getAll() throws Exception {
        throw new BreadCrumbException("Not ALllowed Exception");
    }

    @Override
    public Geofence get(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<Geofence> query(String query) throws Exception {
        return null;
    }

    @Override
    public boolean archive(Geofence query) throws Exception {
        if (query == null) {
            return false;
        }

        if (query.getRequestId() == null) {
            return false;
        }

        ArrayList<String> removeFences = new ArrayList<>();
        removeFences.add(query.getRequestId());

        mGeofencingClient.removeGeofences(removeFences);

        return false;
    }

    @Override
    public boolean unArchive(Geofence query) throws Exception {
        return false;
    }

    @Override
    public Geofence put(GeoFencePutBundle entity) throws SecurityException {
        if (entity == null || !entity.validate()) {
            return null;
        }

        if (mGeofencingClient == null) {
            return null;
        }

        mGeofencingClient.addGeofences(entity.getGeofencingRequest(), entity.getPendingIntent())
                .addOnSuccessListener(entity.getOnSuccessListener())
                .addOnFailureListener(entity.getOnFailureListener());

        return null;
    }

    @Override
    public Geofence post(Geofence entity) throws Exception {
        return null;
    }
}
