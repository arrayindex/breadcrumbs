package com.confusedpenguins.breadcrumbs.domain.usecase.image;

import android.os.Environment;
import android.util.Log;

import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andrew on 1/16/18.
 */

public class CreateImageFileUseCase implements UseCase<File> {
    private String subDirectory;

    public CreateImageFileUseCase(String subDirectory) {
        this.subDirectory = subDirectory;
    }

    @Override
    public File execute() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = String.format("Breadcrumbs_%s_.jpg", timeStamp);

        File imagesFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), subDirectory);
        Log.i(CreateImageFileUseCase.class.getCanonicalName(), "Directory Exists: " + imagesFolder.exists());

        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        return new File(imagesFolder, imageFileName);
    }
}
