package com.confusedpenguins.breadcrumbs.domain.metric;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

/**
 * Created by andrew on 1/7/18.
 */

public class FabricMetricLogger implements MetricLogger {
    private Context context;

    public FabricMetricLogger(Context context) {
        this.context = context;
    }

    @Override
    public boolean logMetric(int stringId) {
        try {
            if (stringId == -1) {
                return false;
            }

            String tag = this.context.getString(stringId);
            if (tag == null) {
                return false;
            }

            Answers.getInstance().logCustom(new CustomEvent(tag));

            String message = String.format("Logged %s", tag);
            Log.d(FabricMetricLogger.class.getCanonicalName(), message);
        } catch (Exception e) {
            Log.e(FabricMetricLogger.class.getCanonicalName(), e.getMessage());
            return false;
        }
        return true;
    }
}
