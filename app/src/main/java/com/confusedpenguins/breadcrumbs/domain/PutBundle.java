package com.confusedpenguins.breadcrumbs.domain;

import java.util.UUID;

/**
 * Created by andrew on 1/9/18.
 */

public abstract class PutBundle {
    private UUID id = UUID.randomUUID();

    public UUID getId() {
        return this.id;
    }

    public abstract boolean validate();
}

