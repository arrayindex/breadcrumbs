package com.confusedpenguins.breadcrumbs.domain.usecase;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumbPutBundle;

/**
 * Created by andrew on 12/30/17.
 */

public class LoadSingleBreadcrumbUseCase implements UseCase<BreadCrumb> {
    private String breadCrumbId;
    Repository<BreadCrumb, BreadCrumbPutBundle> entityController;

    protected LoadSingleBreadcrumbUseCase(Repository<BreadCrumb, BreadCrumbPutBundle> entityController, String breadCrumbId) {
        this.entityController = entityController;
        this.breadCrumbId = breadCrumbId;
    }

    @Override
    public BreadCrumb execute() {
        try {
            if (this.breadCrumbId == null) {
                return null;
            }
            return entityController.get(breadCrumbId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
