package com.confusedpenguins.breadcrumbs.domain.usecase.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;

import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;

/**
 * Created by andrew on 1/18/18.
 */

public class LoadRoundedBitmapDrawableUseCase implements UseCase<RoundedBitmapDrawable> {
    private Resources resources;
    private int width;
    private int height;
    private String mCurrentPhotoPath;

    public LoadRoundedBitmapDrawableUseCase(Resources resources, int width, int height, String mCurrentPhotoPath) {
        this.resources = resources;
        this.width = width;
        this.height = height;
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    @Override
    public RoundedBitmapDrawable execute() {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / width, photoH / height);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, bitmap);
        roundedBitmapDrawable.setCornerRadius(50);
        return roundedBitmapDrawable;
    }
}
