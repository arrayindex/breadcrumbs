package com.confusedpenguins.breadcrumbs.domain.usecase;

/**
 * Created by andrew on 12/30/17.
 */

public interface UseCase<E> {
    E execute();
}
