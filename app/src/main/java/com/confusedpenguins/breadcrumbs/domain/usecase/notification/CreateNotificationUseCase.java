package com.confusedpenguins.breadcrumbs.domain.usecase.notification;

import android.app.PendingIntent;

import com.confusedpenguins.breadcrumbs.R;
import com.confusedpenguins.breadcrumbs.application.notification.BreadCrumbNotificationService;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;

/**
 * Created by andrew on 1/9/18.
 */

public class CreateNotificationUseCase implements UseCase<Boolean> {
    private BreadCrumbNotificationService breadCrumbNotificationService;
    private PendingIntent pendingIntent;
    private BreadCrumb breadCrumb;

    public CreateNotificationUseCase(BreadCrumbNotificationService breadCrumbNotificationService, PendingIntent pendingIntent, BreadCrumb breadCrumb) {
        this.breadCrumbNotificationService = breadCrumbNotificationService;
        this.pendingIntent = pendingIntent;
        this.breadCrumb = breadCrumb;
    }

    @Override
    public Boolean execute() {
        if (this.breadCrumbNotificationService == null) {
            return false;
        }

        if (this.pendingIntent == null) {
            return false;
        }

        if (breadCrumb == null) {
            return false;
        }


        this.breadCrumbNotificationService.postNotification(R.string.notification_geo_title, breadCrumb.getContent(), pendingIntent);
        return true;
    }
}
