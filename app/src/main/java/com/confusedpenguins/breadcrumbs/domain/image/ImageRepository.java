package com.confusedpenguins.breadcrumbs.domain.image;

import android.graphics.Bitmap;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.location.GeoFencePutBundle;
import com.google.android.gms.location.Geofence;

import java.util.ArrayList;

/**
 * Created by andrew on 1/17/18.
 */

public class ImageRepository implements Repository<Bitmap, BitmapPutBundle> {
    @Override
    public ArrayList<Bitmap> getAll() throws Exception {
        return null;
    }

    @Override
    public Bitmap get(String id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<Bitmap> query(String query) throws Exception {
        return null;
    }

    @Override
    public boolean archive(Bitmap query) throws Exception {
        return false;
    }

    @Override
    public boolean unArchive(Bitmap query) throws Exception {
        return false;
    }

    @Override
    public Bitmap put(BitmapPutBundle entity) throws Exception {
        return null;
    }

    @Override
    public Bitmap post(Bitmap entity) throws Exception {
        return null;
    }
}
