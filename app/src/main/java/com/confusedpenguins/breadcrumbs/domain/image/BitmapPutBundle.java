package com.confusedpenguins.breadcrumbs.domain.image;

import android.graphics.Bitmap;

import com.confusedpenguins.breadcrumbs.domain.PutBundle;

/**
 * Created by andrew on 1/17/18.
 */

public class BitmapPutBundle extends PutBundle {
    Bitmap bitmap;

    public BitmapPutBundle(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public boolean validate() {
        if(this.bitmap == null) {
            return false;
        }

        return true;
    }
}
