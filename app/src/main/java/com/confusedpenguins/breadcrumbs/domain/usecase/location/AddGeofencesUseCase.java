package com.confusedpenguins.breadcrumbs.domain.usecase.location;

import android.app.PendingIntent;

import com.confusedpenguins.breadcrumbs.domain.Repository;
import com.confusedpenguins.breadcrumbs.domain.entity.BreadCrumb;
import com.confusedpenguins.breadcrumbs.domain.location.GeoFencePutBundle;
import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

/**
 * Created by andrew on 1/9/18.
 */

public class AddGeofencesUseCase implements UseCase<Void> {
    private Repository<Geofence, GeoFencePutBundle> geoFenceRepository;
    private ArrayList<BreadCrumb> breadCrumbs;
    private PendingIntent pendingIntent;
    private OnSuccessListener onSuccessListener;
    private OnFailureListener onFailureListener;

    public AddGeofencesUseCase(Repository<Geofence, GeoFencePutBundle> geoFenceRepository, ArrayList<BreadCrumb> breadCrumbs, PendingIntent pendingIntent, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        this.geoFenceRepository = geoFenceRepository;
        this.breadCrumbs = breadCrumbs;
        this.pendingIntent = pendingIntent;
        this.onSuccessListener = onSuccessListener;
        this.onFailureListener = onFailureListener;
    }

    @Override
    public Void execute() {
        if (breadCrumbs == null) {
            return null;
        }

        GeofencingRequest geofencingRequest = new CreateGeofencingRequestUseCase(this.breadCrumbs).execute();

        if (geofencingRequest == null) {
            return null;
        }

        if (pendingIntent == null) {
            return null;
        }

        try {
            this.geoFenceRepository.put(new GeoFencePutBundle(geofencingRequest, pendingIntent, onSuccessListener, onFailureListener));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
