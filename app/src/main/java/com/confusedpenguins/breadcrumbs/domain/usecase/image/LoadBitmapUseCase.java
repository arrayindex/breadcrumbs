package com.confusedpenguins.breadcrumbs.domain.usecase.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.confusedpenguins.breadcrumbs.domain.usecase.UseCase;

/**
 * Created by andrew on 1/16/18.
 */

public class LoadBitmapUseCase implements UseCase<Bitmap> {
    private String mCurrentPhotoPath;

    public LoadBitmapUseCase(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    @Override
    public Bitmap execute() {
        if (this.mCurrentPhotoPath == null) {
            return null;
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }
}
