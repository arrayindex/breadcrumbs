package com.confusedpenguins.breadcrumbs.utils;

import android.webkit.URLUtil;

/**
 * Created by Andrew on 7/29/2017.
 */

public class StringUtils {
    public static boolean isUrl(String url) {
        if (url == null) {
            return false;
        }
        return URLUtil.isValidUrl(url);
    }
}
