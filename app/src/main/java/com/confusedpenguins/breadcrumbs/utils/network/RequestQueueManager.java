package com.confusedpenguins.breadcrumbs.utils.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Andrew on 5/2/2017.
 */

public class RequestQueueManager {
    private static RequestQueueManager mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    public static synchronized RequestQueueManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RequestQueueManager(context);
        }
        return mInstance;
    }

    private RequestQueueManager(Context context) {
        if (mInstance == null) {
            this.mCtx = context;
        }
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }
}
